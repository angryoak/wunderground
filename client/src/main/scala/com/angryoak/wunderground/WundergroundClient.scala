/*
 * MIT License
 *
 * Copyright (c) 2018 Daniel Pugliese
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.angryoak.wunderground

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.HttpRequest
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import com.angryoak.wunderground.typed._
import com.angryoak.wunderground.request.HourlyRequest
import spray.json._
import scala.concurrent.{ExecutionContext, Future}

class WundergroundClient(apiKey: String)(implicit actorSystem: ActorSystem, materializer: ActorMaterializer) {

  def getHourly(hourlyRequest: HourlyRequest)(implicit executionContext: ExecutionContext): Future[HourlyForecast] =
    getRawHourly(hourlyRequest).map(HourlyForecast(_))

  def getHourly10Day(hourlyRequest: HourlyRequest)(implicit executionContext: ExecutionContext): Future[HourlyForecast] =
    getRawHourly10Day(hourlyRequest).map(HourlyForecast(_))

  def getRawHourly(hourlyRequest: HourlyRequest)
                  (implicit executionContext: ExecutionContext): Future[raw.HourlyForecast] =
    Http().singleRequest(HttpRequest(uri = hourlyUrl(hourlyRequest.location.requestUrl)))
      .flatMap { resp =>
        Unmarshal(resp).to[String].map { strResp =>
          strResp.parseJson.convertTo[raw.HourlyForecast]
        }
      }

  def getRawHourly10Day(hourlyRequest: HourlyRequest)
                       (implicit executionContext: ExecutionContext): Future[raw.HourlyForecast] =
    Http().singleRequest(HttpRequest(uri = hourly10DayUrl(hourlyRequest.location.requestUrl)))
      .flatMap { resp =>
        Unmarshal(resp).to[String].map { strResp =>
          strResp.parseJson.convertTo[raw.HourlyForecast]
        }
      }

  @inline private def hourlyUrl(requestUrl: String): String =
    s"http://api.wunderground.com/api/$apiKey/hourly/q/$requestUrl.json"

  @inline private def hourly10DayUrl(requestUrl: String): String =
    s"http://api.wunderground.com/api/$apiKey/hourly10day/q/$requestUrl.json"
}
