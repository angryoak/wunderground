/*
 * MIT License
 *
 * Copyright (c) 2018 Daniel Pugliese
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.angryoak.wunderground.typed

import enumeratum._

sealed trait CardinalDirection extends EnumEntry

object CardinalDirection extends Enum[CardinalDirection] {

  override val values = findValues

  case object North extends CardinalDirection
  case object NorthNorthEast extends CardinalDirection
  case object NorthEast extends CardinalDirection
  case object EastNorthEast extends CardinalDirection
  case object East extends CardinalDirection
  case object EastSouthEast extends CardinalDirection
  case object SouthEast extends CardinalDirection
  case object SouthSouthEast extends CardinalDirection
  case object South extends CardinalDirection
  case object SouthSouthWest extends CardinalDirection
  case object SouthWest extends CardinalDirection
  case object WestSouthWest extends CardinalDirection
  case object West extends CardinalDirection
  case object WestNorthWest extends CardinalDirection
  case object NorthWest extends CardinalDirection
  case object NorthNorthWest extends CardinalDirection
  case object Variable extends CardinalDirection

  def fromName(abbrev: String): CardinalDirection =
    withNameInsensitiveOption(abbrev).getOrElse(
      abbreviations.getOrElse(abbrev,
        throw new NoSuchElementException(
          s"$abbrev is not a CardinalDirection abbreviation ${abbreviations.keys.mkString("(", ",", ")")}"))
    )

  private val abbreviations: Map[String, CardinalDirection] = Map(
    "N" -> North,
    "NNE" -> NorthNorthEast,
    "NE" -> NorthEast,
    "ENE" -> EastNorthEast,
    "E" -> East,
    "ESE" -> EastSouthEast,
    "SE" -> SouthEast,
    "SSE" -> SouthSouthEast,
    "S" -> South,
    "SSW" -> SouthSouthWest,
    "SW" -> SouthWest,
    "WSW" -> WestSouthWest,
    "W" -> West,
    "WNW" -> WestNorthWest,
    "NW" -> NorthWest,
    "NNW" -> NorthNorthWest
  )
}

object Direction {

  def apply(raw: com.angryoak.wunderground.raw.Direction): Direction =
    Direction(CardinalDirection.fromName(raw.dir), raw.degrees.toInt)
}

case class Direction(cardinal: CardinalDirection, degrees: Int)
