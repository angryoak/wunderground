/*
 * MIT License
 *
 * Copyright (c) 2018 Daniel Pugliese
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.angryoak.wunderground.typed

import java.time.Instant

object Forecast {

  def apply(raw: com.angryoak.wunderground.raw.Forecast): Forecast =
    Forecast(
      Instant.ofEpochSecond(raw.FCTTIME.epoch.map(_.toLong).getOrElse(0)),
      Temperature(raw.temp),
      Temperature(raw.dewpoint),
      Condition.withName(raw.condition),
      Icon(raw.icon, raw.icon_url),
      raw.fctcode.toInt,
      raw.sky.toInt,
      Speed(raw.wspd),
      Direction(raw.wdir),
      raw.wx,
      raw.uvi.toInt,
      raw.humidity.toInt,
      Temperature(raw.windchill),
      Temperature(raw.heatindex),
      Temperature(raw.feelslike),
      Precipitation(raw.qpf),
      Precipitation(raw.snow),
      raw.pop.toInt,
      Pressure(raw.mslp)
    )
}

case class Forecast(time: Instant,
                    temperature: Temperature,
                    dewpoint: Temperature,
                    condition: Condition,
                    icon: Icon,
                    fctCode: Int,
                    skyCoverage: Int,
                    windSpeed: Speed,
                    windDirection: Direction,
                    wx: String,
                    uvIndex: Int,
                    humidity: Int,
                    windchill: Temperature,
                    heatIndex: Temperature,
                    feelsLike: Temperature,
                    quantitativePrecipitationForecast: Precipitation,
                    snow: Precipitation,
                    percentOfPrecip: Int,
                    meanSeaLevelPressure: Pressure)
