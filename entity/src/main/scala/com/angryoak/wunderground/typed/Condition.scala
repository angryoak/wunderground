/*
 * MIT License
 *
 * Copyright (c) 2018 Daniel Pugliese
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.angryoak.wunderground.typed

import enumeratum.EnumEntry.Camelcase
import enumeratum._

sealed abstract class Condition(override val entryName: String) extends EnumEntry

object Condition extends Enum[Condition] {

  case object Drizzle extends Condition("Drizzle")
  case object Rain extends Condition("Rain")
  case object Snow extends Condition("Snow")
  case object SnowGrains extends Condition("Snow Grains")
  case object IceCrystals extends Condition("Ice Crystals")
  case object IcePellets extends Condition("Ice Pellets")
  case object Hail extends Condition("Hail")
  case object Mist extends Condition("Mist")
  case object Fog extends Condition("Fog")
  case object FogPatches extends Condition("Fog Patches")
  case object Smoke extends Condition("Smoke")
  case object VolcanicAsh extends Condition("Volcanic Ash")
  case object WidespreadDust extends Condition("Widespread Dust")
  case object Sand extends Condition("Sand")
  case object Haze extends Condition("Haze")
  case object Spray extends Condition("Spray")
  case object DustWhirls extends Condition("Dust Whirls")
  case object Sandstorm extends Condition("Sandstorm")
  case object LowDriftingSnow extends Condition("Low Drifting Snow")
  case object LowDriftingWidespreadDust extends Condition("Low Drifting Widespread Dust")
  case object LowDriftingSand extends Condition("Low Drifting Sand")
  case object BlowingSnow extends Condition("Blowing Snow")
  case object BlowingWidespreadDust extends Condition("Blowing Widespread Dust")
  case object BlowingSand extends Condition("Blowing Sand")
  case object RainMist extends Condition("Rain Mist")
  case object RainShowers extends Condition("Rain Showers")
  case object SnowShowers extends Condition("Snow Showers")
  case object SnowBlowingSnowMist extends Condition("Snow Blowing Snow Mist")
  case object IcePelletShowers extends Condition("Ice Pellet Showers")
  case object HailShowers extends Condition("Hail Showers")
  case object SmallHailShowers extends Condition("Small Hail Showers")
  case object Thunderstorm extends Condition("Thunderstorm")
  case object ThunderstormsAndRain extends Condition("Thunderstorms and Rain")
  case object ThunderstormsAndSnow extends Condition("Thunderstorms and Snow")
  case object ThunderstormsAndIcePellets extends Condition("Thunderstorms and Ice Pellets")
  case object ThunderstormsWithHail extends Condition("Thunderstorms with Hail")
  case object ThunderstormsWithSmallHail extends Condition("Thunderstorms with Small Hail")
  case object FreezingDrizzle extends Condition("Freezing Drizzle")
  case object FreezingRain extends Condition("Freezing Rain")
  case object FreezingFog extends Condition("Freezing Fog")
  case object LightDrizzle extends Condition("Light Drizzle")
  case object LightRain extends Condition("Light Rain")
  case object LightSnow extends Condition("Light Snow")
  case object LightSnowGrains extends Condition("Light Snow Grains")
  case object LightIceCrystals extends Condition("Light Ice Crystals")
  case object LightIcePellets extends Condition("Light Ice Pellets")
  case object LightHail extends Condition("Light Hail")
  case object LightMist extends Condition("Light Mist")
  case object LightFog extends Condition("Light Fog")
  case object LightFogPatches extends Condition("Light Fog Patches")
  case object LightSmoke extends Condition("Light Smoke")
  case object LightVolcanicAsh extends Condition("Light Volcanic Ash")
  case object LightWidespreadDust extends Condition("Light Widespread Dust")
  case object LightSand extends Condition("Light Sand")
  case object LightHaze extends Condition("Light Haze")
  case object LightSpray extends Condition("Light Spray")
  case object LightDustWhirls extends Condition("Light Dust Whirls")
  case object LightSandstorm extends Condition("Light Sandstorm")
  case object LightLowDriftingSnow extends Condition("Light Low Drifting Snow")
  case object LightLowDriftingWidespreadDust extends Condition("Light Low Drifting Widespread Dust")
  case object LightLowDriftingSand extends Condition("Light Low Drifting Sand")
  case object LightBlowingSnow extends Condition("Light Blowing Snow")
  case object LightBlowingWidespreadDust extends Condition("Light Blowing Widespread Dust")
  case object LightBlowingSand extends Condition("Light Blowing Sand")
  case object LightRainMist extends Condition("Light Rain Mist")
  case object LightRainShowers extends Condition("Light Rain Showers")
  case object LightSnowShowers extends Condition("Light Snow Showers")
  case object LightSnowBlowingSnowMist extends Condition("Light Snow Blowing Snow Mist")
  case object LightIcePelletShowers extends Condition("Light Ice Pellet Showers")
  case object LightHailShowers extends Condition("Light Hail Showers")
  case object LightSmallHailShowers extends Condition("Light Small Hail Showers")
  case object LightThunderstorm extends Condition("Light Thunderstorm")
  case object LightThunderstormsAndRain extends Condition("Light Thunderstorms and Rain")
  case object LightThunderstormsAndSnow extends Condition("Light Thunderstorms and Snow")
  case object LightThunderstormsAndIcePellets extends Condition("Light Thunderstorms and Ice Pellets")
  case object LightThunderstormsWithHail extends Condition("Light Thunderstorms with Hail")
  case object LightThunderstormsWithSmallHail extends Condition("Light Thunderstorms with Small Hail")
  case object LightFreezingDrizzle extends Condition("Light Freezing Drizzle")
  case object LightFreezingRain extends Condition("Light Freezing Rain")
  case object LightFreezingFog extends Condition("Light Freezing Fog")
  case object HeavyDrizzle extends Condition("Heavy Drizzle")
  case object HeavyRain extends Condition("Heavy Rain")
  case object HeavySnow extends Condition("Heavy Snow")
  case object HeavySnowGrains extends Condition("Heavy Snow Grains")
  case object HeavyIceCrystals extends Condition("Heavy Ice Crystals")
  case object HeavyIcePellets extends Condition("Heavy Ice Pellets")
  case object HeavyHail extends Condition("Heavy Hail")
  case object HeavyMist extends Condition("Heavy Mist")
  case object HeavyFog extends Condition("Heavy Fog")
  case object HeavyFogPatches extends Condition("Heavy Fog Patches")
  case object HeavySmoke extends Condition("Heavy Smoke")
  case object HeavyVolcanicAsh extends Condition("Heavy Volcanic Ash")
  case object HeavyWidespreadDust extends Condition("Heavy Widespread Dust")
  case object HeavySand extends Condition("Heavy Sand")
  case object HeavyHaze extends Condition("Heavy Haze")
  case object HeavySpray extends Condition("Heavy Spray")
  case object HeavyDustWhirls extends Condition("Heavy Dust Whirls")
  case object HeavySandstorm extends Condition("Heavy Sandstorm")
  case object HeavyLowDriftingSnow extends Condition("Heavy Low Drifting Snow")
  case object HeavyLowDriftingWidespreadDust extends Condition("Heavy Low Drifting Widespread Dust")
  case object HeavyLowDriftingSand extends Condition("Heavy Low Drifting Sand")
  case object HeavyBlowingSnow extends Condition("Heavy Blowing Snow")
  case object HeavyBlowingWidespreadDust extends Condition("Heavy Blowing Widespread Dust")
  case object HeavyBlowingSand extends Condition("Heavy Blowing Sand")
  case object HeavyRainMist extends Condition("Heavy Rain Mist")
  case object HeavyRainShowers extends Condition("Heavy Rain Showers")
  case object HeavySnowShowers extends Condition("Heavy Snow Showers")
  case object HeavySnowBlowingSnowMist extends Condition("Heavy Snow Blowing Snow Mist")
  case object HeavyIcePelletShowers extends Condition("Heavy Ice Pellet Showers")
  case object HeavyHailShowers extends Condition("Heavy Hail Showers")
  case object HeavySmallHailShowers extends Condition("Heavy Small Hail Showers")
  case object HeavyThunderstorm extends Condition("Heavy Thunderstorm")
  case object HeavyThunderstormsAndRain extends Condition("Heavy Thunderstorms and Rain")
  case object HeavyThunderstormsAndSnow extends Condition("Heavy Thunderstorms and Snow")
  case object HeavyThunderstormsAndIcePellets extends Condition("Heavy Thunderstorms and Ice Pellets")
  case object HeavyThunderstormsWithHail extends Condition("Heavy Thunderstorms with Hail")
  case object HeavyThunderstormsWithSmallHail extends Condition("Heavy Thunderstorms with Small Hail")
  case object HeavyFreezingDrizzle extends Condition("Heavy Freezing Drizzle")
  case object HeavyFreezingRain extends Condition("Heavy Freezing Rain")
  case object HeavyFreezingFog extends Condition("Heavy Freezing Fog")
  case object PatchesOfFog extends Condition("Patches of Fog")
  case object ShallowFog extends Condition("Shallow Fog")
  case object PartialFog extends Condition("Partial Fog")
  case object Overcast extends Condition("Overcast")
  case object Clear extends Condition("Clear")
  case object PartlyCloudy extends Condition("Partly Cloudy")
  case object MostlyCloudy extends Condition("Mostly Cloudy")
  case object ScatteredClouds extends Condition("Scattered Clouds")
  case object SmallHail extends Condition("Small Hail")
  case object Squalls extends Condition("Squalls")
  case object FunnelCloud extends Condition("Funnel Cloud")
  case object UnknownPrecipitation extends Condition("Unknown Precipitation")
  case object Unknown extends Condition("Unknown")

  override val values = findValues
}
