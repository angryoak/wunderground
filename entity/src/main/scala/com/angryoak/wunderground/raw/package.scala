/*
 * MIT License
 *
 * Copyright (c) 2018 Daniel Pugliese
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.angryoak.wunderground

import spray.json._

package object raw extends DefaultJsonProtocol {

  implicit val directionFormat: RootJsonFormat[Direction] = jsonFormat2(Direction)
  implicit val precipitationFormat: RootJsonFormat[Precipitation] = jsonFormat2(Precipitation)
  implicit val pressureFormat: RootJsonFormat[Pressure] = jsonFormat2(Pressure)
  implicit val speedFormat: RootJsonFormat[Speed] = jsonFormat2(Speed)
  implicit val temperatureFormat: RootJsonFormat[Temperature] = jsonFormat2(Temperature)
  implicit val responseFormat: RootJsonFormat[Response] = jsonFormat3(Response)
  implicit object fctTimeFormat extends RootJsonFormat[FctTime] {
    override def read(json: JsValue): FctTime = json match {
      case JsObject(fields) =>
        FctTime(
          hour = getStringOrNone(fields, "hour"),
          hour_padded = getStringOrNone(fields, "hour_padded"),
          min = getStringOrNone(fields, "min"),
          min_unpadded = getStringOrNone(fields, "min_unpadded"),
          sec = getStringOrNone(fields, "sec"),
          year = getStringOrNone(fields, "year"),
          mon = getStringOrNone(fields, "mon"),
          mon_padded = getStringOrNone(fields, "mon_padded"),
          mon_abbrev = getStringOrNone(fields, "mon_abbrev"),
          mday = getStringOrNone(fields, "mday"),
          mday_padded = getStringOrNone(fields, "mday_padded"),
          yday = getStringOrNone(fields, "yday"),
          isdst = getStringOrNone(fields, "isdst"),
          epoch = getStringOrNone(fields, "epoch"),
          pretty = getStringOrNone(fields, "pretty"),
          civil = getStringOrNone(fields, "civil"),
          month_name = getStringOrNone(fields, "month_name"),
          month_name_abbrev = getStringOrNone(fields, "month_name_abbrev"),
          weekday_name = getStringOrNone(fields, "weekday_name"),
          weekday_name_night = getStringOrNone(fields, "weekday_name_night"),
          weekday_name_abbrev = getStringOrNone(fields, "weekday_name_abbrev"),
          weekday_name_unlang = getStringOrNone(fields, "weekday_name_unlang"),
          weekday_name_night_unlang = getStringOrNone(fields, "weekday_name_night_unlang"),
          ampm = getStringOrNone(fields, "ampm"),
          tz = getStringOrNone(fields, "tz"),
          age = getStringOrNone(fields, "age"),
          UTCDATE = getStringOrNone(fields, "UTCDATE")
        )
      case other => throw DeserializationException(s"JsObject expected, but got $other")
    }

    override def write(obj: FctTime): JsValue = ???

    @inline
    private def getStringOrNone(fields: Map[String, JsValue], key: String): Option[String] = {
      fields.get(key).flatMap{
        case JsString(str) => Some(str)
        case other => None
      }.map(_.toString).filter(_.nonEmpty)
    }
  }
  implicit val forecastFormat: RootJsonFormat[Forecast] = jsonFormat20(Forecast)
  implicit val hourlyForecastFormat: RootJsonFormat[HourlyForecast] = jsonFormat2(HourlyForecast)
}
