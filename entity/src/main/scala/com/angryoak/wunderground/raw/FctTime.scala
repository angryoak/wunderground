/*
 * MIT License
 *
 * Copyright (c) 2018 Daniel Pugliese
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.angryoak.wunderground.raw

case class FctTime(hour: Option[String],
                   hour_padded: Option[String],
                   min: Option[String],
                   min_unpadded: Option[String],
                   sec: Option[String],
                   year: Option[String],
                   mon: Option[String],
                   mon_padded: Option[String],
                   mon_abbrev: Option[String],
                   mday: Option[String],
                   mday_padded: Option[String],
                   yday: Option[String],
                   isdst: Option[String],
                   epoch: Option[String],
                   pretty: Option[String],
                   civil: Option[String],
                   month_name: Option[String],
                   month_name_abbrev: Option[String],
                   weekday_name: Option[String],
                   weekday_name_night: Option[String],
                   weekday_name_abbrev: Option[String],
                   weekday_name_unlang: Option[String],
                   weekday_name_night_unlang: Option[String],
                   ampm: Option[String],
                   tz: Option[String],
                   age: Option[String],
                   UTCDATE: Option[String])
