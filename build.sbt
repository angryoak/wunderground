
val commonSettings = Seq(
  organization := "com.angryoak",
  version := "0.0.1-SNAPSHOT",
  scalaVersion := "2.12.6"
)

val testSettings = Seq(
  libraryDependencies ++= Seq(
    "org.scalatest" %% "scalatest" % "3.0.5" % Test
  )
)

lazy val akkaStreamVersion = "10.1.4"

lazy val wunderEntity = (project in file("entity"))
  .settings(
    commonSettings,
    testSettings,
    name := "wunderground.entity",
    libraryDependencies ++= Seq(
      "com.beachape" %% "enumeratum" % "1.5.13",
      "com.typesafe.akka" %% "akka-http-spray-json" % akkaStreamVersion
    )
  )

lazy val wunderClient = (project in file("client"))
  .settings(
    commonSettings,
    testSettings,
    name := "wunderground.client",
    libraryDependencies ++= Seq(
      "com.typesafe.akka" %% "akka-stream" % "2.5.12",
      "com.typesafe.akka" %% "akka-http" % akkaStreamVersion
    )
  )
  .dependsOn(wunderEntity)